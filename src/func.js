const getSum = (str1, str2) => {
  if(typeof str1 != 'string' || typeof str2 != 'string'){
    return false;
  }
  if((str1.match(/^[0-9]+$/) == null && str1.length!=0)|| (str2.match(/^[0-9]+$/) == null&& str2.length!=0)){
    return false;
  }
  return parseInt(str1.length==0?"0":str1) + parseInt(str2.length==0?"0":str2) +"";
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let commentsNumber = 0;
  for(let i = 0; i<listOfPosts.length; i++){
    if(listOfPosts[i].author === authorName){
      posts++;
    }
    if(listOfPosts[i].comments == null) continue;
    for(let j=0; j<listOfPosts[i].comments.length; j++){
      if(listOfPosts[i].comments[j].author===authorName){
        commentsNumber++;
      }
    }
  }
  return "Post:"+posts+",comments:"+commentsNumber;
};

const tickets=(people)=> {
  let sum = 0;
  for(let i = 0; i < people.length; i++){
    if(people[i]==25) {
      sum+=25;
    }
    else if(people[i]==50){
      if(sum>=25){
        sum+=25;
      }
      else{
        return "NO";
      }
    }
    else{
      if(sum>=75){
        sum+=25;
      }
      else{
        return "NO"
      }
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
